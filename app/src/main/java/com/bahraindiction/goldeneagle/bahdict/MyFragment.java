package com.bahraindiction.goldeneagle.bahdict;

import android.content.Intent;
import android.graphics.Typeface;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Administrator on 27-Jan-16.
 */
public class MyFragment extends Fragment implements View.OnClickListener {


    public static MyFragment newInstance() {
        MyFragment fragment = new MyFragment();
        return fragment;
    }


    public MyFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.my_fragment, container, false);


        TextView title = (TextView) rootView.findViewById(R.id.textView2);
        TextView title2 = (TextView) rootView.findViewById(R.id.textView3);
        Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/janna.ttf");
        title.setTypeface(font);
        title2.setTypeface(font);

        ImageButton twitt, insta, mail;
        twitt = (ImageButton) rootView.findViewById(R.id.twitterbutton);
        insta = (ImageButton) rootView.findViewById(R.id.instabutton);
        mail = (ImageButton) rootView.findViewById(R.id.mailbutton);
        twitt.setOnClickListener(this);
        insta.setOnClickListener(this);
        mail.setOnClickListener(this);


        return rootView;
    }


    @Override
    public void onClick(View v) {
        //do what you want to do when button is clicked
        switch (v.getId()) {
            case R.id.twitterbutton:
                TwittAccess();
                break;
            case R.id.instabutton:
                InstaAccess();
                break;
            case R.id.mailbutton:
                EmailMailer();
                break;


        }
    }

    public void EmailMailer(){
       final ImageButton btnmail = (ImageButton)getView().findViewById(R.id.mailbutton);
        btnmail.setImageResource(R.drawable.mailiconclick);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                btnmail.setImageResource(R.drawable.mailicon);
            }
        }, 0500);

        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"g3agle@gmail.com"});
        i.putExtra(Intent.EXTRA_SUBJECT, "شكوى، استفسار، أو إضافة لمحتوى تطبيق المعجم البحريني");
        //i.putExtra(Intent.EXTRA_TEXT   , "body of email");
        try {
            startActivity(Intent.createChooser(i, "إرسال إيميل عبر..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getContext(), "There are no Email clients installed", Toast.LENGTH_LONG).show();
        }


    }

    public void InstaAccess(){

        final ImageButton btnInsta = (ImageButton)getView().findViewById(R.id.instabutton);
        btnInsta.setImageResource(R.drawable.instaconclick);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                btnInsta.setImageResource(R.drawable.instacon);
            }
        }, 0500);

        String url = "https://www.instagram.com/haethamal7addad/";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);

    }

    public void TwittAccess(){

        final ImageButton btntwitt = (ImageButton)getView().findViewById(R.id.twitterbutton);
        btntwitt.setImageResource(R.drawable.twiticonclick);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                btntwitt.setImageResource(R.drawable.twiticon);
            }
        }, 0500);

        String url2 = "https://twitter.com/eaglegamer47";
        Intent h = new Intent(Intent.ACTION_VIEW);
        h.setData(Uri.parse(url2));
        startActivity(h);

    }


}
