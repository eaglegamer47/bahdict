package com.bahraindiction.goldeneagle.bahdict;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;

import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by Administrator on 27-Jan-16.
 */
public class AboutFragment extends ListFragment {

    static String item;


    public static AboutFragment newInstance() {
        AboutFragment fragment = new AboutFragment();
        return fragment;


    }


    public AboutFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.aboutfragment, container, false);





        String[] values = new String[]{"أوتي", "امبرطم", "أم الديفان", "استن", "أجلح أملح", "المكدة", "اللابلة", "امبلتع",
                "بقة", "بادقير", "بطبطة", "بقعة", "بجي", "بقشة", "بزخ", "تحنقل", "تغلغص", "تفق", "تخادن", "ترس", "تلح", "جكويتو", "جندس", "جوكم",
                "جعص", "حمقي", "خريش", "خولجحلة", "خنة", "خنينة", "داعوس", "دبج", "دعله", "درعم", "ديغي", "ذلف", "ربشه", "راشى", "ريري",
                "زنجفرة", "زرنوق", "زتات", "زقر", "سابعة", "سبعه", "سهدة", "سندارة", "سكسبال", "شحقه", "شهست", "شنقايل", "شلوشن",
                "شخاط", "شنهو", "صخة", "صنج", "ضكة", "طاسة", "طنقر", "طشت", "طرطور", "عفطي", "عمبلوص", "عفر", "عباب", "عباله", "عنبوه",
                "قرمبع", "قلعه", "كوفنه", "كلكجي", "كفخ", "لفد", "لولب", "مفهي", "ميش / ماميش", "مقاقة", "محبس", "مجثي", "محتر", "متدوده",
                "ملوص", "ملوت", "ماجلة", "محر", "مش بوزك", "مكوّك", "نايبة", "نغزة", "هايت", "همزة", "همجة", "هيس", "هتلي", "وطب", "ودر",
                "يبحلق / يخوزر", "يخوره", "يهد"


        };
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, values);
        setListAdapter(adapter);


        return rootView;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        item = ((TextView) v).getText().toString();



        //  Toast.makeText(getContext(), item, Toast.LENGTH_LONG).show();

        Intent intent = new Intent(AboutFragment.this.getActivity(), Shower.class);
        startActivity(intent);


    }
}
